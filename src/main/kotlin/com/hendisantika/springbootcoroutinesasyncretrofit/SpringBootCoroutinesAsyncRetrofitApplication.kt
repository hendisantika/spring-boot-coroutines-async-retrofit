package com.hendisantika.springbootcoroutinesasyncretrofit

import com.fasterxml.jackson.databind.ObjectMapper
import com.hendisantika.springbootcoroutinesasyncretrofit.service.JsonPlaceHolderRegularClient
import com.hendisantika.springbootcoroutinesasyncretrofit.service.JsonPlaceHolderSuspendClient
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

@SpringBootApplication
class SpringBootCoroutinesAsyncRetrofitApplication

@Bean
fun jsonPlaceHolderRegularClient(): JsonPlaceHolderRegularClient {
    return Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com")
        .addConverterFactory(JacksonConverterFactory.create(ObjectMapper().findAndRegisterModules()))
        .build()
        .create(JsonPlaceHolderRegularClient::class.java)
}

@Bean
fun jsonPlaceHolderSuspendClient(): JsonPlaceHolderSuspendClient {
    return Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com")
        .addConverterFactory(JacksonConverterFactory.create(ObjectMapper().findAndRegisterModules()))
        .build()
        .create(JsonPlaceHolderSuspendClient::class.java)
}

fun main(args: Array<String>) {
    runApplication<SpringBootCoroutinesAsyncRetrofitApplication>(*args)
}
