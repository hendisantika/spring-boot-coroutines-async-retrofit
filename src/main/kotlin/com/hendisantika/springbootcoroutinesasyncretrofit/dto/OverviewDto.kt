package com.hendisantika.springbootcoroutinesasyncretrofit.dto

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-coroutines-async-retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/05/21
 * Time: 07.37
 */
data class OverviewDto(
    val posts: List<PostDto>,
    val todos: List<TodoDto>,
)
