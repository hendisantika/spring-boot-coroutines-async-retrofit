package com.hendisantika.springbootcoroutinesasyncretrofit.dto

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-coroutines-async-retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/05/21
 * Time: 07.29
 */
data class PostDto(
    val userId: Long,
    val id: Long,
    val title: String,
    val body: String,
)
