package com.hendisantika.springbootcoroutinesasyncretrofit.service

import com.hendisantika.springbootcoroutinesasyncretrofit.dto.PostDto
import com.hendisantika.springbootcoroutinesasyncretrofit.dto.TodoDto
import org.springframework.stereotype.Service
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-coroutines-async-retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/05/21
 * Time: 07.30
 */
@Service
interface JsonPlaceHolderSuspendClient {
    @GET("/posts/{id}")
    suspend fun post(@Path("id") id: Int): PostDto

    @GET("/todos/{id}")
    suspend fun todo(@Path("id") id: Int): TodoDto
}
