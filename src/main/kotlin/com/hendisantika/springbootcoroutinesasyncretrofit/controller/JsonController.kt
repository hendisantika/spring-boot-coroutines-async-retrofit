package com.hendisantika.springbootcoroutinesasyncretrofit.controller

import com.hendisantika.springbootcoroutinesasyncretrofit.dto.OverviewDto
import com.hendisantika.springbootcoroutinesasyncretrofit.dto.PostDto
import com.hendisantika.springbootcoroutinesasyncretrofit.dto.TodoDto
import com.hendisantika.springbootcoroutinesasyncretrofit.service.JsonPlaceHolderRegularClient
import com.hendisantika.springbootcoroutinesasyncretrofit.service.JsonPlaceHolderSuspendClient
import kotlinx.coroutines.*
import org.apache.logging.log4j.LogManager.getLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import retrofit2.awaitResponse

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-coroutines-async-retrofit
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/05/21
 * Time: 07.38
 */
@RestController
class JsonController(
    @Autowired val jsonPlaceHolderSuspendClient: JsonPlaceHolderSuspendClient,
    @Autowired val jsonPlaceHolderRegularClient: JsonPlaceHolderRegularClient
) {
    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        private val log = getLogger(javaClass.enclosingClass)
    }

    @GetMapping("/overview-suspend")
    suspend fun getOverviewSuspend(): OverviewDto {
        log.info("fetching overview")
        // switch to the IO context for asynchronous IO related work!
        return withContext(Dispatchers.IO) {
            val postRequests: List<Deferred<PostDto>> = (1..5).map { id ->
                async {
                    log.info("fetching post $id")
                    jsonPlaceHolderSuspendClient.post(id).also {
                        log.info("done fetching post $id")
                    }
                }
            }
            val fiveTodosRequests: List<Deferred<TodoDto>> = (1..5).map { id ->
                async {
                    log.info("fetching todo $id")
                    jsonPlaceHolderSuspendClient.todo(id).also {
                        log.info("done fetching todo $id")
                    }
                }
            }

            // .awaitAll() maps over the list and calls await() on each Deferred object. It also cancelles
            // all the other tasks that are still rune if one of them fails!
            val postResponses = postRequests.awaitAll()
            val todoResponses = fiveTodosRequests.awaitAll()

            OverviewDto(
                posts = postResponses,
                todos = todoResponses,
            )
        }.also {
            log.info("done!")
        }
    }

    @GetMapping("/overview-regular")
    suspend fun getOverviewRegular(): OverviewDto {
        log.info("fetching overview regular")
        return withContext(Dispatchers.IO) {
            val fivePosts = (1..5).map { id ->
                async {
                    log.info("fetching post $id")
                    jsonPlaceHolderRegularClient.post(id).awaitResponse().body()!!.also {
                        log.info("done fetching post $id")
                    }
                }
            }
            val fiveTodos = (1..5).map { id ->
                async {
                    log.info("fetching todo $id")
                    jsonPlaceHolderRegularClient.todo(id).awaitResponse().body()!!.also {
                        log.info("done fetching todo $id")
                    }
                }
            }

            OverviewDto(
                posts = fivePosts.awaitAll(),
                todos = fiveTodos.awaitAll(),
            )
        }
    }
}
