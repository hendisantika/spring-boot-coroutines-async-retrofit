# Spring Boot CoRoutines Async Retrofit

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-coroutines-async-retrofit`
2. Navigate to the folder: `cd spring-boot-coroutines-async-retrofit`
3. Run the application: `gradle clean bootRun --info`
4. Open your favorite browser then copy these url:

* http://localhost:8080/overview-suspend
* http://localhost:8080/overview-regular

For more details about the explanation. Please click the source article:
* https://blog.jdriven.com/2020/11/async-retrofit-with-coroutines-and-spring-boot
* https://betterprogramming.pub/how-to-fire-and-forget-kotlin-coroutines-in-spring-boot-40f8204aac86
